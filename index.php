<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
spl_autoload_register(function($className) {
	// $path = rtrim(__DIR__, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'htdocs' . DIRECTORY_SEPARATOR; 
	$path = rtrim(__DIR__, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR; 
	$fullPath = $path . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php'; 
	if(file_exists($fullPath)) { 
			require_once $fullPath; 
		} 
});




$Ford = new \MyProject\Products\AutoCar('Ford');   // новая машина марки Ford
$Toyota = new \MyProject\Products\AutoCar('Toyota');  // новая машина марки Toyota
$BMW = new \MyProject\Products\AutoCar('BMW');    // новая машина марки BMW
$BMW->color = 'red';        // BMW покрасили красным цветом 
$BMW->setType('passenger');  //  BMW сделали легковым авто
echo 'тип автомашины: ' . $BMW->getType() . '<br />';    // узнали, что BMW - легковая
echo 'поворот налево на: ' . $BMW ->courseNew(45, 5) . '<br />';    // повернули руль на 45 град. и держим 5 сек. - получили новый курс
echo $BMW ->speed(5, 3) . ' км/ч' . '<br />';     // надавили на газ усилием в 5 ед. и получили скорость
echo 'всего машин: ' . \MyProject\Products\AutoCar::$staticCounter;    // определили кол-во новых авто
echo '<br /><br />'; 


$tv_LG = new \MyProject\Products\Televisor();       // новый телевизор LG
$tv_samsung = new \MyProject\Products\Televisor();    // новый телевизор Samsung
$tv_samsung->category = 'бытовая техника';    // 
$tv_samsung->name = 'телевизор'; 
$tv_samsung->setBrand('samsung');    // марка - samsung
$tv_samsung->setType('LSD TV');    // тип - жидкокристаллический
$tv_samsung->setDiagonal('127cm');    // диагональ - 127 сантиметров
$tv_samsung->setPrice(50000);        // назначили цену
$tv_samsung->setDiscount(10);      // назначили скидку
echo 'категория: ' . $tv_samsung->category . '<br />';      // узнали марку
echo 'телевизор: ' . $tv_samsung->getBrand() . '<br />';      // узнали марку
echo $tv_samsung->getType() . '<br />';    // узнали тип
echo 'диагональ: ' . $tv_samsung->getDiagonal() . '<br />';  // узнали диагональ
echo $tv_samsung->getPrice() . ' руб. <br />';  // узнали цену
$tv_samsung->turnOn();   // нажали на кнопку и включили
echo '<br /><br />'; 


$parker = new \MyProject\Products\Ballpen();    // новая ручка
$rayter = new \MyProject\Products\Ballpen();    // новая ручка
$rayter->setBrand('rayter');    // марка новой ручки - rayter
$rayter->setType('gel');    // тип новой ручки - гелевая
$rayter->turnOn();      // нажал на кнопку на обратном концце ручки, видно стержень
$rayter->turnOff();      // нажал на кнопку, стержень не видно
echo 'марка авторучки: ' . $rayter->getBrand() . '<br />';   //  узнал марку ручки
echo 'тип авторучки: ' .  $rayter->getType() . '<br />';    // узнал тип - гелевая
var_dump($rayter ->getRod());    // вынул стержень
echo '<br />';
var_dump($rayter ->insertRod()) . '<br />';    // вставил стержень
echo '<br />';
echo $rayter ->write('Hello world!');    // написал слова
echo '<br /><br />'; 


$Krya = new \MyProject\Products\Duck('Krya');      // новая утка, зовут Кря
$Donald = new \MyProject\Products\Duck('Donald');    // новая утка, зовут Дональд
$Donald ->floats();          // Дональд плавает
$Donald ->walking();       // Дональд ходит
$Donald ->fly();          // Дональд летает
echo 'утка в воде? '; 
var_dump($Donald ->onTheAir); 
echo '<br />'; 
echo $Donald ->name;   // узнаем имя Дональда
echo '<br /><br />'; 


$basket = new \Selling\Basket();            // новая корзина
$basket ->addProduct($tv_samsung, 1);            // добавляю товар в корзину
$basket ->addProduct($tv_samsung, 2);             // добавляю товар в корзину
echo 'удаляю товар №4 из корзины <br />' . $basket ->deleteProduct(4) . '<br /><br />';   // удаляю товар из корзины
echo 'товаров в корзине на сумму: ' . $basket ->getSumma() . ' руб. <br /><br />';    // вывод суммы товаров

$adres = new \Selling\Adres('Россия', '123456', 'Красноярск', 'ул. Кирова', 'д. 51', 'оф. 618', '8-123-456-78-90', 'email@email.job'); 
$adres->region = 'Красноярский край'; 

$order = new \Selling\Order($basket);    // новый заказ
$order ->ordering('Иван', 'Иванов', 1, $adres);   // оформление заказа
print 'Информация о заказе: <br />'; 
foreach ( $order ->printing() as $key => $string_for_print) {     // вывод информации о заказе
	print $string_for_print . '<br />'; 
} 