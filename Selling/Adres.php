<?php 
namespace Selling;            // пространство имен Продажи
error_reporting(E_ALL);        //вывести на экран все ошибки


class Adres   // класс адрес
{
	public $land; 
	public $post_index; 
	public $region; 
	public $city; 
	public $street; 
	public $building; 
	public $office; 
	public $telephone; 
	public $email; 

	function __construct($land, $post_index, $city, $street, $building, $office, $telephone, $email) 
	{
		$this->land = $land; 
		$this->post_index = $post_index; 
		$this->city = $city; 
		$this->street = $street; 
		$this->building = $building; 
		$this->office = $office; 
		$this->telephone = $telephone; 
		$this->email = $email; 
	} 
} 