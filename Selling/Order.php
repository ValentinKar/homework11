<?php 
namespace Selling;       // пространство имен Продажи
use Selling\Basket;      // использую класс корзина
use Selling\Adres;          // использую класс адрес
error_reporting(E_ALL);        //вывести на экран все ошибки


class Order       // класс заказ
{ 
	private $basket;      // товары в корзине
	private $id;             // номер заказа
	private $buyerFirstName; 
	private $buyerLastName; 
	private $adres; 
	private $print = []; 

	public function __construct(Basket $basket)  // обязательный параметр - корзина
	{ 
		$this->basket = $basket; 
	} 

	public function getBasket()      // показать корзину
	{
		return $this->basket; 
	} 

	public function ordering($FirstName, $LastName, $id, Adres $adres)      // оформить заказ
	{
		$this ->buyerFirstName = $FirstName; 
		$this ->buyerLastName = $LastName; 
		$this ->id = $id; 
		$this ->adres = $adres;
		return true; 
	} 

	public function printing()      // вывод информации о заказе
	{  
		$i = 1; 
		foreach ( $this ->basket->printProducts() as $key=>$value ) {
			$this ->print[] = $i . '. ' . $value[ 0 ] ->name . ' ' . $value[0] ->getBrand() . ' (' . $value[0] ->category . '); цена: ' . $value[0] ->getPrice() . ' руб.;  кол-во: ' . $value[1] ; 
			$i++; 
		} 
		array_push($this ->print, 'Итого: ' . $this ->basket->getSumma() . ' руб. ', 
		PHP_EOL, 
		'Заказчик: ' . $this ->buyerFirstName. ' ' . $this ->buyerLastName, 
		$this ->adres ->region, 
		'Город: ' . $this ->adres ->city, 
		$this ->adres ->street, 
		$this ->adres ->building, 
		$this ->adres ->office 
		); 
		return $this ->print; 
	}

} 