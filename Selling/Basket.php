<?php 
namespace Selling;           // пространство имен продажи
error_reporting(E_ALL);        //вывести на экран все ошибки


class Basket       // класс корзина
{ 
	private $products = [];      // товары в корзине
	public $number = 1;         // номер товара

	public function addProduct($product, $sum_product)  // добавляю товар в корзину
	{
		$this->products[ $this ->number ] = [
				$product,                       // товар в корзине
				$sum_product                   // кол-во товара в корзине
			]; 
		$this ->number++;                  // номер товара в корзине
		return true; 
	} 

	public function deleteProduct($number)     // удаляю товар из корзины 
	{
		if ( key_exists( $number, $this ->products ) )  { 
			unset( $this ->products[$number] ); 
			return true; 
		} 
		else { 
			return 'заказа с номером id=' . $number . ' нет в корзине'; 
			die;
		} 
	} 

	public function getSumma()      // вывод суммы заказа
	{ 
			$result = 0; 
			foreach ( $this->products as $id => $product ) 
			{
				$result = $result + ( $product[0]->getPrice() * $product[1] ) ; 
			} 
			return $result;     // сумма заказа
	} 

	public function printProducts()     // вывожу информацию о товарах в корзине
	{ 
		return $this->products; 
	} 
}