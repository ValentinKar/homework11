<?php 
namespace MyProject\Products; 
use MyProject; 
use Modules; 
error_reporting(E_ALL);        //вывести на экран все ошибки


class Ballpen extends MyProject\Product implements Modules\Writers, Modules\WithRemoteControl // шариковая ручка
{
	private $rod = true;  // есть стержень или нет
	private $ink = 100;  // сколько чернил осталось в стержне
	public $work = true;  // работает или нет - если есть кнопка как у старых авторучек

	public function turnOn()   // включить
	{
		$this->work = true; 
		return $this ->work;
	}

	public function turnOff()   // выключить
	{
		$this->work = false; 
		return $this->work; 
	}

		public function insertRod()    // вставить стержень
		{
			$this ->rod = true; 
			return $this ->rod;
		}

		public function getRod()    // достать стержень
		{
			$this ->rod = false; 
			return $this ->rod;
		}

	public function write($text)    // написать текст
	{ 
			if ($this ->rod === false) {
				exit;
			}

		$i = strlen($text); 
		$this ->turnOn(); 

			if ($this ->type = 'gel') {
			$this ->ink = $this ->ink - $i / 5;
			}

			if ($this ->type = 'roller') {
			$this ->ink = $this ->ink - $i / 10;
			}

		echo "$text"; 
		return ' (чернил осталось: ' . $this ->ink . ' ед.) <br />'; 
	}
}