<?php 
namespace MyProject\Products; 
use MyProject; 
use Modules; 
error_reporting(E_ALL);        //вывести на экран все ошибки


class AutoCar extends MyProject\Product implements Modules\Transport   // машина
{
	public static $staticCounter = 0;   // статическое свойство - счетчик

	private $speed_max = 150;  // максимальная скорость
	public $course = 0;  // направление движения машины
	public $angle_rudder = 0;  // угол поворота руля
	public $speed = 0;      // скорость
	public $gas = 0;    // подача топлива

	public function __construct($brand)  // конструктор устанавливает марку
	{
		$this->brand = $brand; 
		self::$staticCounter++; 
	}

	public function courseNew($rotation_rudder, $time)  // изменяю направление движения
	{
		$this->angle_rudder = $this ->angle_rudder + $rotation_rudder; 
		$this->course = $this ->course + $this ->angle_rudder / 10 * $time; 
		return $this ->course;
	}

	public function speed($add_gas, $time)   // изменяю скорость движения
	{
		$this->gas = $this ->gas + $add_gas;
		$this->speed = $this ->speed + $this ->gas * 10; 
		return $this ->speed; 
	}
}