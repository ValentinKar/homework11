<?php 
namespace MyProject\Products;   
use MyProject; 
use Modules;  
error_reporting(E_ALL);        //вывести на экран все ошибки


class Duck extends MyProject\Product implements Modules\Birds  // утка
{
	public $name; 
	public $onTheWater = true;   // на воде
	public $onTheLand = false;   // на суше
	public $onTheAir = false;      // в воздухе


	public function __construct($krya)  // конструктор требует имя
	{
		$this->name = $krya; 
	}

		public function floats()  // плывет
		{
			$this->onTheWater = true;
			$this ->onTheLand = false;
			$this ->onTheAir = false;
		}

	public function walking()  // идет
	{
		$this ->onTheLand = true;
		$this->onTheWater = false;
		$this ->onTheAir = false;
	}

		public function fly()  // летит
		{
			$this ->onTheAir = true;
			$this ->onTheLand = false;
			$this->onTheWater = false;
		}
}