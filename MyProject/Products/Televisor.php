<?php 
namespace MyProject\Products; 
use MyProject; 
use Modules; 
error_reporting(E_ALL);        //вывести на экран все ошибки


class Televisor extends MyProject\Product implements Modules\WithRemoteControl 
{
	private $diagonal;  // диагональ
	public $work = false;  // работает или нет

	public function setDiagonal($diagonal)  // изменяю диагональ телевизора
	{
		$this->diagonal = $diagonal; 
	} 

	public function getDiagonal()   // узнаю диагональ телевизора
	{
		return $this->diagonal; 
	} 

		public function turnOn()   // включить телевизор
		{
			$this->work = true; 
			return $this -> work;
		}

		public function turnOff()   // выключить телевизор
		{
			$this->work = false; 
			return $this->work; 
		}
}