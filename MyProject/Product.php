<?php 
namespace MyProject;         // пространство имен MyProject
error_reporting(E_ALL);        //вывести на экран все ошибки


abstract class Product         // продукт
{
	public $category; // категория
	public $name;      // имя товара
	protected $brand;  // марка
	public $color;  // цвет 
	protected $price; // цена
	protected $discount; // скидка
	protected $type;    // тип товара

		public function setPrice($price)  // изменяю цену
		{
			$this->price = $price;
		}

	public function getPrice()   // узнаю цену
	{
		if ($this ->discount) {
			return round($this->price - ($this->price * $this ->discount/100) );
		}
		else {
			return $this ->price;
		}
	}

		public function setDiscount($discount)  // изменяю цену
		{
			$this->discount = $discount;
		}

	public function getDiscount()   // узнаю цену
	{	
			return $this ->discount;
	}

	public function setBrand($brand)  // изменяю марку авторучки
	{
		$this->brand = $brand;
	}

	public function getBrand()   // узнаю марку авторучки
	{
		return $this->brand;
	}

		public function setType($type)  // изменяю тип авторучки
		{
			$this->type = $type; 
		} 

		public function getType()   // узнаю тип авторучки
		{
			return $this->type; 
		} 

	
}