<?php 
namespace Modules;
error_reporting(E_ALL);        //вывести на экран все ошибки


interface Transport  // средства передвижения
{
	public function courseNew($rotation_rudder, $time);  // сменить курс
	public function speed($add_gas, $time);   // сменить скорость
}