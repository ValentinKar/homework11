<?php 
namespace Modules;
error_reporting(E_ALL);        //вывести на экран все ошибки


interface WithRemoteControl  // с дистанционным управлением
{
	public function turnOn(); 
	public function turnOff(); 
}